function r = AlignRecordings(fromEGG, fromHeadmounted)
% ALIGNRECORDINGS align two recordings, usually coming from the mike from the
% EGG machine, and the other coming from a head-mounted mike. Only about 
% the first minute of the recordings is considered if they're longer. The
% reference for the alignment is the acoustic recording since that's the
% one used for annotation.
%
% SYNOPSIS: AlignRecordings(fromEGG, fromHeadmounted, aligned)
%
% INPUT fromEGG: the wav file from the EGG
%       fromHeadmounted: the wav from the head-mounted
%       aligned: a directory where to save the aligned wavefiles
%
% OUTPUT r: correlation of the recordings. Aligned recordings will be saved
% in the 'aligned' directory.
%
% REMARKS The EGG recording may contain external noises, results should be checked.
%
% AUTHOR    : Julian Villegas
% $DATE     : 09-Mar-2018 13:53:59 $
% $Revision : 1.00 $
% DEVELOPED : 9.3.0.713579 (R2017b)
% FILENAME  : AlignRecordings.m

if (~nargin)
    fromEGG = ''; % most be stereo, with the acoustic signal in the left channel
    fromHeadmounted = '';
end
% load the stereo wav file
 [a,sra] = audioread(fromHeadmounted);
 % change this to 60 so it's a minute
 maxSamples = sra*60; % First minute 
 sizeA = size(a);
 if sizeA(2) ~= 1
     a = a(:,1); % assuming the recording is in the left channel
 end
 
 [e,sre] = audioread(fromEGG);
 sizeE = size(e);
  if sizeE(2) ~= 2
     error('The EGG is not stereo, please fix'); 
 end
 
 if(sra~=sre)
     error('The two files have different sampling rates, please fix');
 else
     sr = sra;
 end
 
[cor,~] = xcorr(a(1:maxSamples),e(1:maxSamples,1)); % the acoustic signal is in the left channel
[~,d] = max(abs(cor));
itd = (d - maxSamples);

subplot(2,1,1)
plot(.5*a(1:maxSamples/2)/max(abs(a(1:maxSamples/2)))+.5)
hold on
stem(itd,.5,'r')
plot(.5*e(1:maxSamples/2,1)/max(abs(e(1:maxSamples/2,1)))-.5)
alpha(.5)
hold off

if itd >= 0
    e = [zeros(itd,2); e];
else  
    e = e(abs(itd)+1:end,:);
end

% adjust the length of e
sizeE = size(e);
if sizeE(1)>= sizeA(1)
    e = e(1:sizeA(1),:);
else
    e = [e; zeros(sizeA(1)-sizeE(1),2)];
end
    

subplot(2,1,2)
plot(.5*a(1:maxSamples/2)/max(abs(a(1:maxSamples/2)))+.5)
hold on
plot(.5*e(1:maxSamples/2,1)/max(abs(e(1:maxSamples/2,1)))-.5)
alpha(.5)
hold off

% Prepare recordings for Eggnog
audiowrite([fromEGG(1:end-4) '_aligned.wav'],[a,e(:,2)],sr);

end
% ===== EOF ====== [AlignRecordings.m] ======
