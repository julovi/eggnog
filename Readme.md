# Description
This program reads EGG recordings from a directory and returns files that can be imported in R for further analysis.

# How to use
The easiest way to use Eggnog is to download the installer from the "Downloads" section. Once downloaded it, double click on it and it will install two things: 1) the Matlab runtime machine and 2) the Application itself (Eggnog.app). The actual directory where the App is installed depends on your configuration. Double-click on the "Eggnog.app" to run the application.
If you decide *not* to use the installer, you need to download the source code. Then, you can run Egnog using the `Eggnog.mlapp` app.

Open `Eggnog.mlapp` and follow the instructions therein. There is a set of recordings in the subdirectory `testRecording` to try out. 

To choose informed values for the peak detection routine, please read the file `peakdet_shortguide.pdf` in the `documentation`

In the subdirectory `auxiliarScripts` you can find programs for helping in the process of analyzing EGG recordings. For example, `AlignRecordings.m` helps to time-align acoustic and EGG recordings when they're perfomed simultaneously but in different devices.

## Note about the parallel toolbox 
If you have downloaded the source code, as opposed to the binaries, you need to have the parallel toolbox installed in Matlab. If you don't have it, please manually change the `parfor` circa line 147 of `Eggnog_guts.m` to a simple `for`.

# License
ReadPraatTier.m is a slightly modified version of the same routine found in mview (Mark Tiede, tiede@haskins.yale.edu). The license of mview is uncertain.

Other functions were copied from Covarep (Gilles Degottex, degottex@csd.uoc.gr). These  are covered with a LGPL license. 

# Authors
Created by Julián Villegas  (julian at u-aizu.ac.jp) and Camilo Arévalo
University of Aizu, 2017-2023.
