function Eggnog_guts(srcDir, dstDir,doPlot,dEGG,rEGG,zoomPlot,shortLabel,autoF0,maxF0,prefilter,tier,label,EGGChannel,EGGConvention,fileAsColumn,EggnogReport,doublePeakMethod,smoothingstep,dr)
% This function reads EGG recordings from a directory and returns a file
% that can be imported in R for further analysis
%
% The directory must contain stereo recordings with audio in a channel, and
% EGG in the other. It must also have a Textgrid with labels on the regions
% of interest.
%
% Inputs:
% - the directory where the EGG recordings are (srcDir)
% - the directory where you want to put the plots and traces (dstDir)
% - Do plot? (doPlot; 0: No, 1: Yes)
% - If so, plot the derivative of the EGG signal? (dEGG; 0: No, 1: Yes)
% - Plot also the EGG signal itself? (plotting dEGG and rEGG is not recommended) (rEGG; 0: No, 1: Yes)
% - restrict the plot area to the max value of the boundaries? (zoomPlot; 0: No, 1: Yes)
% - abbreviate the labels in the plot when too long? (shortLabel; 0: No, 1: Yes)
% - Guess automatic maximum expected F0? (only use 1 if the acousticrecording is good) (autoF0; 0: No, 1: Yes)
% - What's the maximum expected F0? (if autoF0 is 1, this value is not used.) (MaxF0)
% - Low-pass filter noisy EGGs? (prefilter; 0: No, 1: Yes)
% - What's the tier name or number? (tier;'seg', 'segment', etc. or 1,2,3, etc.)
% - what's the label of interest? (label; use '*' to match all existing labels)
% - Which channel is EGG?(EGGChannel; 1: Left, 2: Right)
% - Is it closure up (VFCA) or closure down (IVFCA)?
%   If unsure, check the spectrogram and EG  signal (e.g., in Praat), dark zones
%   correspond to apertures, white ones to closures.
%   (EGGConvention; VFCA: 1, IVFCA: 2)
% - Include the filename as a column? (filesAsColumn; 0: No, 1: Yes)
% - Include a report with the hitherto parameters for postmortem analysis (EggnogReport; 0: No, 1: Yes)
% - Double peak method: To do an informed decision, please read peakdet_shortguide.pdf in the documentation folder.
% - A smoothing step of 1 is adequate for high-quality signals
% - Dynamic range for the spectrograms

% Parameter to modify:
% - Whether to plot or not (doPlot)
% - the tier name (tier)
% - the label you want from the tier (label, an * match all the labels)
% - Whether the EGG is the left or right channel of the recording
%   (EGGChannel, in case of monophonic recording, i.e., when EGG was
%    recorded independently of the acoustic signal use 1)
% - Whether the glottis closure is up (VFCA) or down (IVFCA) in
%   the egg plot (EGGConvention)
% - Which method to use for the extraction (method)
%
% Default values are provided when possible.
%
%
%
% Julian Villegas
% University of Aizu, 2017.
% V 0.2: Fixed a problem with Matlab versions < 9.1
% V 0.3: Fixed some visualization problems (narrowband spectrogram, etc.)
% V 0.6: Add post-mortem report
% V 0.7: Add controls for spectrogram, method to deal with double peaks,
% and smoothing of the EGG signal.

EggnogVersion = '0.7';
f = filesep;

% Change the following lines according to your needs
% From here ____________________________________________________________

if nargin==0
    srcDir = ['.' f 'testRecording' f];
end
if nargin<=1
    dstDir = srcDir;
end
if nargin<=2
    doPlot = 1;
end
if nargin<=3
    dEGG = 0;
end
if nargin<=4
    rEGG = 1;
end
if nargin<=5
    zoomPlot = 1;
end
if nargin<=6
    shortLabel = 0;
end
if nargin<=7
    autoF0 = 1;
end
if nargin<=8
    maxF0 = 500;
end
if nargin<=9
    prefilter = 0;
end
if nargin<=10
    tier = 1;
end
if nargin<=11
    label = '*';
end
if nargin<=12
    EGGChannel = 2;
end
if nargin<=13
    EGGConvention = 2 ;
end
if nargin<=14
    fileAsColumn = 1;
end
if nargin<=15
    EggnogReport = 1;
end
% peakdet parameters:
% ------
% To do an informed decision, please read peakdet_shortguide.pdf in the
% documentation folder
if nargin<=16
    doublePeakMethod=3; % 1..4
    %                 0: selecting highest of the peaks
    %                 1: selecting first peak
    %                 2: selecting last peak
    %                 3: using barycentre method
    %                 4: exclude all double peaks
end
if nargin<=17
    smoothingstep=3;% 0..10
    % a smoothing step of 1 is adequate for high-quality signals
    % (which already appear visually as very smooth), a smoothing step of 2 or 3
    % increases correct peak detection in relatively noisy signals. Fudging-up of
    % opening peaks was only observed with a smoothing step of 6 or more.
    % ------
end
if nargin<=18
    dr=50; % dynamic range for the spectrograms
end

% To here ____________________________________________________________

srcDir = [srcDir f];
dstDir = [dstDir f];
sourceFiles=dir([srcDir '*.TextGrid']);
numberOfFiles=length(sourceFiles);

% convert the label string into cell array
label = textscan(label, '%s', 'delimiter', ',');
label = label{1};
label = regexprep(label, '"', '');


parfor i=1:numberOfFiles
    nm = sourceFiles(i).name;
    disp([num2str(i) ' ' nm])
    flag = 0; % Flag if there are errors in the computations such as negative OQ, etc.s
    % load the stereo wav file
    [x,sr] = audioread([srcDir nm(1:end-8) 'wav']);
    if ismac
        [~, o2] = system(['file -I ' [srcDir nm]]);
        [~,encoding] = strtok(o2, '=');
        encoding = encoding(2:end-1);
        if strcmpi(encoding(1:5),'utf-1') || strcmpi(encoding(1:5),'binar')
            copyfile([srcDir nm],[srcDir nm '.WrongUTF']);
            e = system(['iconv -f ' encoding ' -t utf-8 ' srcDir nm '>' srcDir nm '.newUTF']);
            if e
                fileDir = [dstDir 'excluded' f];
                if ~exist(fileDir, 'dir')
                    s = mkdir(fileDir);
                    if ~s
                        error('unable to create the file directory');
                    end
                end
                movefile([srcDir nm],[fileDir nm]);
                movefile([srcDir nm(1:end-8) 'wav'],[fileDir nm(1:end-8) 'wav']);
                delete([srcDir nm '.WrongUTF']);
                continue;
            else
                movefile([srcDir nm '.newUTF'],[srcDir nm]);
            end
        end
    end

    % load the Praat TextGrid
    [segs,labs] = ReadPraatTier([srcDir nm],tier);
    if isempty(labs)
        disp(['Warning: Didn''t find the labs for file ' nm '.']);
    end

    % Extract EGG and Audio
    if EGGChannel == 1
        egg = x(:,1);
        wav = x(:,2);
    else
        egg = x(:,2);
        wav = x(:,1);
    end
    % Although Covarep tells you to use the signal closure-up, in my own
    % tests with creaky, modal and breathy voice, the correct results are
    % when the signal is closure-down.
    if EGGConvention == 1
        egg = -egg;
    end

    if autoF0
        maxF0 = 1.5*estimateF0(wav,sr); %#ok<PFTIN>
    end

    if prefilter
        N = 3;  % order
        cutoff_Hz = 2.1*maxF0;
        [b,a]=butter(N,cutoff_Hz/(sr/2),'low');
        egg = filtfilt(b,a,egg);
    end
    try
        [results_matrix,~,~] = peakdet(egg, sr, maxF0, ...
            doublePeakMethod, smoothingstep);
    catch
        results_matrix = NaN;
    end

    % matrix contains:
    % - beginning and end of period : in 1st and 2nd columns in secondds
    %- F0 : 3rd column
    %- DECPA, Derivative-Electroglottographic Closure Peak Amplitude:
    %         4th column (on DECPA and DEOPA: see [3])
    %- Oq determined from raw maximum, and DEOPA : 5-6th col [1,2]
    %- Oq determined from maximum after smoothing : 7th col [1,2]
    %- Oq determined from peak detection : 8-9th col without
    %  smoothing and with smoothing.
    %
    % To make time centered in each frame
    if ~isnan(results_matrix)
        t = 0.5*(results_matrix(:,1)+results_matrix(:,2));
        %t = results_matrix(:,1);
        f0 =  results_matrix(:,3);
        % Im not sure which one gives better results 7 or 9?
        Oq = round(results_matrix(:,9)/100,3);

        if verLessThan('matlab', '9.1')
            row = ones(length(t),1);
            for j=1:size(segs,1)
                blah = find(t>segs(j,1) & t<=segs(j,2))';
                row(blah) = j;
            end
        else
            blah = find(t'>segs(:,1) & t'<=segs(:,2));
            [row, ~] = ind2sub(size(segs),blah);
        end

        labels = labs(row);
        if sum(size(labels)==1)
            if size(labels,2) > size(labels,1)
                labels = labels';
                flag = 1; % they should be column vectors, results shouldn't be trusted
            end
        else
            disp('the label array is not a vector');
            flag = 1;
        end


        times = floor([segs(row,1), segs(row,2)]*1000);
        lenT = length(t);
        if length(labels) < lenT
            labels = [labels; cell(lenT-length(labels),1)];
        end
        if length(times) < lenT
            times = [times; cell(lenT-length(times),1)];
        end
        [lblr,~] = size(labels);
        if lblr==1
            labels = labels';
        end
        if sum((Oq<0 | Oq > 1)) || sum(f0<0)
            flag = 1; % there were errors in the computation, the results shouldnt be trusted
        end

        if flag
            fname = ['_' nm(1:end-8) 'csv'];
        else
            fname = [nm(1:end-8) 'csv'];
        end

        fileDir = [dstDir 'EggnogTraces' f];
        if ~exist(fileDir, 'dir')
            s = mkdir(fileDir);
            if ~s
                error('unable to create the file directory');
            end
        end

        if length(Oq)> 1 && ~flag
            if fileAsColumn
                m = table(round(t*1000,1), Oq, round(f0,1), labels, ...
                    repmat(fname(1:end-4), length(Oq), 1), times(:,1), times(:,2), ...
                    'VariableNames',{'Time','OpenQ','F0','Label','Filename','SegIni','SegEnd'});
            else
                m = table(round(t*1000,1), Oq, round(f0,1), labels, times(:,1), times(:,2), ...
                    'VariableNames',{'Time','OpenQ','F0','Label','SegIni','SegEnd'});
            end

            % Save only the labels of interest
            if ~strcmpi(label,'*')
                m(~ismember(m.Label,label),:)=[];
            end
            writetable(m,[fileDir fname]);

        else
            disp('Not enough data to create table')
        end


        % Plot
        if doPlot
            try
                plotAll(t,Oq,f0,segs,labs,wav,egg,sr,dstDir,nm,zoomPlot,shortLabel,1.1*max(f0), dEGG,rEGG, dr);
            catch
                disp('could not create plot\n');
            end
        end

    else
        fileDir = [dstDir 'excluded' f];
        if ~exist(fileDir, 'dir')
            s = mkdir(fileDir);
            if ~s
                error('unable to create the file directory');
            end
        end
        movefile([srcDir nm],[fileDir nm]);
        movefile([srcDir nm(1:end-8) 'wav'],[fileDir nm(1:end-8) 'wav']);
    end

end
msgbox('Operation Completed');
% Report
if EggnogReport
    fid = fopen([dstDir 'Eggnog_Report.txt'],'w');
    fprintf(fid,'Eggnog Version: %s \n',EggnogVersion);
    fprintf(fid,'Date: %s \n', string(datetime('now','Format','d-MMM-y HH:mm:ss')));
    fprintf(fid,'Directory: %s \n',srcDir);
    fprintf(fid,'Number of files: %d \n',numberOfFiles);
    fileDir = [dstDir 'excluded' f];
    if ~exist(fileDir, 'dir')
        numExcluded = 0;
    else
        exFiles=dir([fileDir '*.TextGrid']);
        numExcluded=length(exFiles);
    end
    fprintf(fid,'Number of excluded files: %d \n',numExcluded);
    fprintf(fid,'Auto F0: %d \n',autoF0);
    fprintf(fid,'Prefilter: %d \n',prefilter);
    fprintf(fid,'Tier: %d \n',tier);
    label = [label,[repmat({','},numel(label)-1,1);{[]}]]';
    label = [label{:}];
    fprintf(fid,'Label: %s \n',label);
    fprintf(fid,'Egg channel: %d \n',EGGChannel);
    closure = 'na';
    if EGGConvention == 1
        closure = 'up';
    elseif EGGConvention == 2
        closure = 'down';
    end
    fprintf(fid,'Egg closure: %s \n',closure);
    fclose(fid);
end
end

function F0 = estimateF0(x,sr)
% Design a bandpass filter to remove frequencies outside the expected F0 range.
% Adjust the filter order and frequency range as needed.
[b0,a0] = butter(3 ,[75 500]/(sr/2), 'bandpass');
x = filter(b0,a0,x);
Nfft = length(x);
[Pxx,f] = pwelch(x,gausswin(Nfft),Nfft/2,Nfft,sr);
[~,loc] = max(Pxx);
F0 = f(loc);
end

function plotAll(t,Oq,f0,segs,labs,phrase,egg,sr,dstDir,nm,zoomPlot,shortLabel,maxF0,dEGG,rEGG, dr)
linesAreOn = 0;
markSize = 3;
hold off
plotDir = fullfile(dstDir, 'EggnogPlots');
if ~exist(plotDir, 'dir')
    s = mkdir(plotDir);
    if ~s
        error('unable to create the plot directory');
    end
end

phrase10=resample(phrase,10000,sr);
phrase10 = phrase10/max(abs(phrase10));

if zoomPlot
    maxX = max(segs( :,1))+.01;
    minX = min(segs( :,2))-.01;
else
    maxX = length(phrase10)/10000;
    minX = 0;
end

preemph = 0.97; % this gives a +6 dB/Oct, ~50 Hz @ sr = 10kHz
phrase10 = filter([1, -preemph],1, phrase10);
phrase10 = phrase10/max(abs(phrase10));

N = round(0.005*10000);
[S,F,T] = spectrogram(phrase10,gausswin(N),round(3*N/4),64,10000,'yaxis'); %
clf
BA=20 * log10(abs(S));
BAM=max(BA);
BAmax=max(BAM);
BA(BA < BAmax-dr)=BAmax-dr;

% Spectrogram
subplot(3, 1, 1);
%yyaxis left
surf(T, F, BA)
shading interp
colorbar off
view([0 90])
axis tight
%axis xy;
a = gray(1000);
colormap(1 - a)
axis([minX maxX 0 5000])
y=ylabel('Hz');
set(y,'FontSize',12,'FontWeight','bold')
line([segs(:,1) segs( :,1)],get(gca,'YLim'),'Color','k',...
    'LineStyle', '--')

yyaxis right
p=plot(t,f0,'ok');
hold on
line(t,f0)
hold off

axis([minX maxX 20 maxF0])
y=ylabel('F0/Hz');
set(y,'FontSize',12,'FontWeight','bold')
set(p,'Color',[0 0.5 0.5],'MarkerSize',markSize)
set(gca,'YGrid','on')

tt = title(nm(1:end-9),'interpreter','none');
set(tt,'FontSize',16,'FontWeight','bold')

% Egg
subplot(3, 1, 2);
nt = linspace(0,length(egg)/sr,length(egg));

if rEGG
    p= plot(nt,-egg);
    limy = round(1.1 * max(abs(egg)), 1);
    if limy == 0
        limy = 1.1 * max(abs(egg));
    end

    axis([minX maxX -limy limy])
    set(p,'Color','b','MarkerSize',markSize)
    y=ylabel('Egg (closure up)');
    line([segs(:,1) segs(:,1)],get(gca,'YLim'),'Color','k',...
        'LineStyle', '--')
    linesAreOn = 1;
    set(y,'FontSize',12,'FontWeight','bold')
    set(gca,'YGrid','on','YTick',-limy:0.2:limy)
end

if dEGG
    yyaxis right
    hold on
    deregg = diff(-egg);
    p= plot(nt(1:end-1),deregg);
    limy = round(1.1 * max(abs(deregg)),1);

    if limy == 0
        limy = 1.1*max(abs(deregg));
    end

    axis([minX maxX -limy limy])
    set(p,'MarkerSize',markSize)
    hold off
    y=ylabel('dEgg (closure up)');

    if ~linesAreOn
        line([segs(:,1) segs(:,1)],get(gca,'YLim'),'Color','k',...
            'LineStyle', '--')
    end

    set(y,'FontSize',12,'FontWeight','bold')
end
xticks([])

% open quotient
subplot(3, 1, 3);
p=plot(t, Oq, 'ok');
hold on
line(t,Oq)
hold off
axis([minX maxX 0 1])
x=xlabel('t/s');
set(x,'FontSize',12,'FontWeight','bold')
y=ylabel('Oq');
set(y,'FontSize',12,'FontWeight','bold')
set(p,'Color','b','MarkerSize',markSize)
set(gca,'YGrid','on','YTick',0:0.2:1)
line([segs(:,1) segs(:,1)],get(gca,'YLim'),'Color','k',...
    'LineStyle', '--')

if shortLabel
    labs = regexprep(labs,'.....$|\n','...');
end

text(mean(segs,2),1.02*ones(length(segs),1),labs,'Rotation',90,...
    'interpreter','none')

fig = gcf;
fig.PaperUnits = 'inches';
fig.PaperPosition= [0 0 6 4];
fig.PaperSize = [6 4];
print(fig, [plotDir  filesep nm(1:end-8) 'pdf'], '-vector', '-dpdf', '-r150');
end


